
(define (minOfThree x y z)
	(cond	((and (< x y) (< x z)) x) 
		((and (< y x) (< y z)) y)
		((and (< z y) (< z x)) z)
	)
	
)

(define (sumOfSquares x y)
	(+ (* y y) (* x x))
  )

(define (sumOfTwoLargestSquared x y z)
  	;sum the squares of the two largest
	(cond 	((and (< x y) (< x z)) 
	       		(sumOfSquares y z) )
		((and (< y x) (< y z))
		 	(sumOfSquares x z) )
		((and (< z y) (< z x)) 
		 	(sumOfSquares y x) )
		(else (sumOfSquares x y) )
	)
)
