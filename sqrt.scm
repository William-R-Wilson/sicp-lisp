
(define (min-of-three x y z)
	(cond	((and (< x y) (< x z)) x) 
		((and (< y x) (< y z)) y)
		((and (< z y) (< z x)) z)
	)
	
)

(define (abs x)
        (cond ((> x 0) x)
              ((= x 0) 0)
              ((< x 0) (- x))

              )
	)


(define (square x) 
  	(* x x) )

(define (sumOfSquares x y)
	(+ (square x) (square y))
  )

(define (average x y)
  	(/ (+ x y) 2)
	)

(define (good-enough? guess x)
	(< (abs (- (square guess) x)) 0.001)
  )

(define (improve guess x)
  	(average guess (/ x guess))
)

(define (sqrt-iter guess x)
	(if (good-enough? guess x)
	    guess
	    (sqrt-iter (improve guess x) x))
  )

(define (sqrt x)
	(sqrt-iter 1.0 x)
)
